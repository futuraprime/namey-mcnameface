# namey-mcnameface

Generate short, memorable random names for stuff

```js
const nameyMcNameface = require( 'namey-mcnameface' );

const name1 = nameyMcNameface(); // 'warlike-guppy'
const name2 = nameyMcNameface(); // 'loose-haddock'
const name3 = nameyMcNameface(); // 'furtive-mackerel'
const name4 = nameyMcNameface(); // 'late-caribou'
const name5 = nameyMcNameface(); // 'agitated-lungfish'
const name6 = nameyMcNameface(); // 'quirky-opossum'
// and so on...
```

Install it with `npm install namey-mcnameface` or [download from npmcdn.com](https://npmcdn.com/namey-mcnameface).

## License

MIT
